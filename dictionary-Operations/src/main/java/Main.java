import Utils.Utils;
import dictionaryOperations.AnagramsOperations;
import dictionaryOperations.DictionaryOperation;
import dictionaryOperations.PalindromeDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperation;
import fileReader.ResourceInputFileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("This is my main class");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        //String userInput = in.readLine();
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicate(words);
        while (true) {
            System.out.println("menu: \n" +
                    "1. Search \n" +
                    "2. Find all palindromes \n" +
                    "3. Anagrame \n" +
                    "0. Exit");
            String userInput = in.readLine();
            if (userInput.equals("0")) {
                System.out.println("Goodbye");
                break;
            }
            DictionaryOperation operation = null;
            switch (userInput) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsNoDuplicate, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsNoDuplicate);
                    break;
                case "3":
                    operation = new AnagramsOperations(wordsNoDuplicate, in);
                    break;
                default:
                    System.out.println("invalid option");
            }
            if (operation != null) {
                operation.run();
            }
        }
//        System.out.println("This is the user input: " + userInput);
//        for (String word : words) {
//            System.out.println(word);
//        }
//
    }
}