package Utils;
import java.lang.reflect.Array;
import java.util.*;

public class Utils {
    public static Set<String> removeDuplicate(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();
        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }
    public static Set<String> findWordsThatContainsASpecificString(Set<String> wordsSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordsSet) {
            if (line.contains(word)) {
                result.add(line);
            }
        }
        return result;
    }
    public static Set<String> findAllPalindromes(Set<String> wordsSet){
        Set<String> result = new HashSet<>();
        for (String line:wordsSet){
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if(line.equals(reverseWord.toString())){
                result.add(line);
            }
        }
        return result;
    }
    //------------------------------------------------------------------------
    public static String sortChar(String word){
//        char [] charArray = word.trim().toCharArray();
//            Arrays.sort(charArray); // a e t
//            String sortedWord = String.valueOf(charArray); //eat
        char[] a = word.toLowerCase().toCharArray();
            Arrays.sort(a); // a e t
            String sortedWord = new String(a); //eat
        return sortedWord;
    }
    public static Map<String, List<String>>findAllAnagrams(Set<String> wordsSet){
       Map<String, List<String>> anagrams = new HashMap<>();
//        Map<String, List<String>> anagrams = null;
//        if(wordsSet == null || wordsSet.size() == 0){
//            return null;
//        }
//        anagrams = new HashMap<>();
        String[] words1 = wordsSet.toArray(new String[0]);
        for (String i : words1){
            //e.g eat --- next three line breaks the worts in letters,
            // arrange the letters alphabetically and group them as words again
//            char [] charArray = word.trim().toCharArray();
//            Arrays.sort(charArray); // a e t
//            String sortedWord = String.valueOf(charArray); //eat
            String word = String.valueOf(i);
            String sortedCharWord = sortChar(word);

            if (anagrams.containsKey(sortedCharWord)){
                List<String> newList = anagrams.get(sortedCharWord);
                newList.add(word);
                anagrams.put(sortedCharWord, newList);
                //ArrayList <String> values = new ArrayList<>(newList);
            }
            else{
                List<String> list = new ArrayList<String>();
                list.add(word);
                anagrams.put(sortedCharWord, list);
               // ArrayList <String> values = new ArrayList<>(list);
            }
        }
        return anagrams;
    }
}
