package dictionaryOperations;
import Utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.*;
public class AnagramsOperations implements DictionaryOperation {
    private Set<String> wordsSet;
    private BufferedReader in;

    public AnagramsOperations(Set<String> wordsSet, BufferedReader in) {
       // List<String> words1 = new ArrayList<>(hashCode());
        {
            this.wordsSet = wordsSet;
            this.in = in;
        }
    }

    @Override
    public void run() throws IOException {
        Map<String, List<String>> anagrams = Utils.findAllAnagrams(wordsSet);
        System.out.println("Anagram: ");
//        List<String> anagrams = (List<String>) Utils.findAllAnagrams((List<String>) wordsSet);
        String userInput = in.readLine();
        String sortedWord = Utils.sortChar(userInput);
        if (anagrams.containsKey(sortedWord)) {
            anagrams.get(sortedWord).add(userInput);
        } else {
            System.out.println("Wrong Input");
        }
        System.out.println(anagrams.get(sortedWord));
    }
}


