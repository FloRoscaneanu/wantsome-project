import Utils.Utils;
import fileReader.ResourceInputFileReader;
import org.junit.Assert;
import org.junit.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class UtilsTests {
    @Test
    public void testPalindromeFinder(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("unu");
        Set<String> palindromes = Utils.findAllPalindromes(inputSet);
        Assert.assertTrue(palindromes.contains("unu"));
        Assert.assertEquals(1, palindromes.size());
    }
    @Test
    public void testPalindromesFromFile() throws IOException {
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicate(words);
        Set<String> palindromes = Utils.findAllPalindromes(wordsNoDuplicate);
        Assert.assertTrue(palindromes.contains("aba"));
    }
}
